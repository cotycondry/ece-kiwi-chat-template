var sessionId = "";
App.Cobrowse = Backbone.Model.extend({
    cbWndHandleArr: new Array(),
    initialize: function () {
		//bind onlclick handler for handling join cobrowse links
		$(document).ready(function() {
			$(document).on('click', '#egcb_join_link', function(event) {
				event.preventDefault();
				var aTag = event.target ;
				sessionId = $(aTag).attr('egcb_sessionId') ;
				var winName = $(aTag).attr('egcb_winName') ;
				var href = $(aTag).attr('egcb_href') ;
				$(aTag).attr('target', winName) ;
			
				openCobrowseWindow(href, winName, sessionId) ;
			});
		});			
        App.session.on('end', this.stopCobrowseSession, this);       
    },
    stopCobrowseSession : function() {
	try{
		var egActId = "";
	    var sid = App.connection.sid;
		if(sid){
	    	egActId = sid.substr(0 , sid.indexOf('_'));
	    }
	    var vhSId = vhtIds?vhtIds.sId:"";
	    if(sessionId || egActId || vhSId){
			localStorage.setItem('endCbSession', sessionId+'$eg$'+egActId+'$eg$'+vhSId);
	    }
	}catch(error){}
    },
    stopCobrowseCallback : function(cbSessionId){
		this.cbWndHandleArr[cbSessionId].status = false;
    },
    
    openCobrowseWindow: function(url, cbWndName, cbSessionId) {
	//check if window already open
	if(this.cbWndHandleArr[cbSessionId])	
	{
			if(this.cbWndHandleArr[cbSessionId].status==false || (this.cbWndHandleArr[cbSessionId].handle && this.cbWndHandleArr[cbSessionId].handle.closed))
			alert(L10N_COBROWSE_SESSION_TERMINATED);
		else
			alert(L10N_COBROWSE_SESSION_ALREADY_JOINED);
	}
	else
	{		
		var sid = App.connection.sid;
		var egActId="";
		if(sid){
		 	egActId = sid.substr(0 , sid.indexOf('_'));
	    }
		localStorage.setItem('egActId',egActId);
		localStorage.setItem('cbAutoSessionId',cbSessionId);
		var d = getDimensions(CONFIG_LOGIN_FORM_WIDGET_URL);
		// w,h -- -2: let browser decide, -1: take up available space, +ve values: use them
		var w = eGainLiveConfig.cobrowseWindowWidth || -2;
		var h = eGainLiveConfig.cobrowseWindowHeight || -2;
		if (w == -1) {
			w = window.screen.availWidth - d.width - 15; // 15: to avoid windows going out of screen to some extent - cross browser.
		}
		if (h == -1) {
			h = window.screen.availHeight;
		}
		var l = d.width;
	
		var options = 'resizable=yes, scrollbars=yes, top=0';
		if (l > 0)
			options += ', left='+l;
		if (w > 0)
			options += ', width='+w;
		if (h > 0)
			options += ', height='+h;
		var wndHandle = window.open(url, cbWndName, options);
		this.cbWndHandleArr[cbSessionId] = {status:true, handle:wndHandle};		
		top.moveTo(0, 0);
		if(wndHandle)
			wndHandle.focus();		
	}
    }    
});

